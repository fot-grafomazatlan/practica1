package com.example.practica0192;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnPulsar;
    private Button btnErase;
    private Button btnExit;
    private EditText txtNombre;
    private TextView lblSaludar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //relacionar los objetos
        btnPulsar = (Button) findViewById(R.id.btnSaludar);
        btnErase = (Button) findViewById(R.id.btnBorrar);
        btnExit = (Button) findViewById(R.id.btnSalir);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar = (TextView) findViewById(R.id.lblSaludo);

        //Codificar el evento clic del boton

        btnPulsar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //validar

                if (txtNombre.getText().toString().matches("")) {

                    Toast.makeText(MainActivity.this, "Falto de capturar la informacion",
                            Toast.LENGTH_SHORT).show();
                }else {
                    String str = "Hola " + txtNombre.getText().toString() + ", ¿Como estas?";
                    lblSaludar.setText(str);
                }
            }
        });
        btnErase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Botón Borrar
                lblSaludar.setText("");
                txtNombre.setText("");
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Boton Salir
                finish();
            }
        });

    }
}